{-# LANGUAGE PatternSynonyms #-}

------------------------------------------------------------------------------
-- |
-- Module      : Data.Boolean.Fake
-- Copyright   : (C) Patrick Suggate, 2020
-- License     : BSD3
--
-- Maintainer  : Patrick Suggate <patrick.suggate@gmail.com>
-- Stability   : Experimental
-- Portability : non-portable
--
-- Boolean-type that is more conveniently stored within @Vector@ arrays.
--
-- == Changelog
--  - 19/09/2020  --  initial file;
--
-- == TODO
--  - add some instances;
--
------------------------------------------------------------------------------

module Data.Boolean.Fake
  (
    B
  , pattern F
  , pattern T
  )
where

import           GHC.Int (Int8)


-- * Fake booleans
------------------------------------------------------------------------------
-- | Uses 8x less storage than @Bool@'s.
type B = Int8

pattern F :: B
pattern F  = 0

pattern T :: B
pattern T  = 1
