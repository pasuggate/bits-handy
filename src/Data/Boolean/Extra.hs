{-# LANGUAGE ConstraintKinds, DefaultSignatures, FunctionalDependencies, GADTs,
             MultiParamTypeClasses #-}

------------------------------------------------------------------------------
-- |
-- Module      : Data.Boolean.Extra
-- Copyright   : (C) Patrick Suggate 2016
-- License     : BSD3
--
-- Maintainer  : Patrick Suggate <patrick.suggate@gmail.com>
-- Stability   : Experimental
-- Portability : non-portable
--
-- Some more Boolean functions.
--
-- Changelog:
--  + 10/02/2016  --  initial file;
--  + 14/03/2016  --  refactored into separate library;
--  + 18/09/2020  --  refactored and improved the INLINE's;
--
------------------------------------------------------------------------------

module Data.Boolean.Extra
  (
    -- Re-exports
    module Data.Boolean

    -- Constraints
  , BoolB
  , BoolBit

    -- Type-classes
  , ExtraB (..)
  , ReduceB (..)
  , ShiftB (..)

    -- Reducers
  , andB, nandB
  , orB, norB
  , xorB, xnorB

    -- Additional functions
  , (%>>)
  , (%<<)
  , lenB
  , halfAdder
  , fullAdder
  )
where

import           Data.Bool
import           Data.Boolean


-- * Unified Boolean constraints.
------------------------------------------------------------------------------
type BoolB   b = (Boolean b, ExtraB b)
type BoolBit b = (BooleanOf b ~ b, BoolB b)


-- * Set the operator fixities.
------------------------------------------------------------------------------
-- TODO: What should the fixity of XOR be?
infixr 3 &&~
infixr 2 ^^*, ||~, ^^~


-- * Extra Boolean functions.
------------------------------------------------------------------------------
-- | These should've been included within `Boolean`.
class ExtraB b where
  (^^*) :: b -> b -> b          -- ^ XOR
  (&&~) :: b -> b -> b          -- ^ NAND
  (||~) :: b -> b -> b          -- ^ NOR
  (^^~) :: b -> b -> b          -- ^ XNOR

  -- ^ Default instances can be made if `b` has a `Boolean` instance.
  default (^^*) :: Boolean b => b -> b -> b
  a ^^* b = (a ||* b) &&* (a &&~ b)

  default (&&~) :: Boolean b => b -> b -> b
  a &&~ b = notB $ a &&* b

  default (||~) :: Boolean b => b -> b -> b
  a ||~ b = notB $ a ||* b

  default (^^~) :: Boolean b => b -> b -> b
  a ^^~ b = notB $ a ^^* b

------------------------------------------------------------------------------
-- | Reduction operators.
class ReduceB t where
-- class ReduceB hom t | t -> hom where -- TODO:
  andR  :: t -> BooleanOf t
  orR   :: t -> BooleanOf t
  xorR  :: t -> BooleanOf t
  nandR :: t -> BooleanOf t
  norR  :: t -> BooleanOf t
  xnorR :: t -> BooleanOf t

------------------------------------------------------------------------------
-- | Bit-shift operators.
--   TODO: Fixities and symbols.
infixl 8 <<%, >>%

class ShiftB amount word | word -> amount where
  (<<%) :: word -> amount -> word
  (>>%) :: word -> amount -> word

(%<<) :: ShiftB amount word => amount -> word -> word
(%<<)  = flip (<<%)
{-# INLINE (%<<) #-}

(%>>) :: ShiftB amount word => amount -> word -> word
(%>>)  = flip (>>%)
{-# INLINE (%>>) #-}

{-- }
-- NOTE: This is too general-purpose, and breaks some functionality in other
--   modules.
instance (Foldable t, BooleanOf (t b) ~ b, Boolean b, ExtraB b) => ReduceB (t b) where
  andR  = andB
  orR   = orB
  xorR  = xorB
  nandR = nandB
  norR  = norB
  xnorR = xnorB
--}


-- * Additional Boolean operators.
------------------------------------------------------------------------------
andB  :: (Foldable t, Boolean b) => t b -> b
andB   = foldl1 (&&*)
{-# INLINE andB  #-}

orB   :: (Foldable t, Boolean b) => t b -> b
orB    = foldl1 (||*)
{-# INLINE orB   #-}

-- | A parity function.
xorB  :: (Foldable t, ExtraB b) => t b -> b
xorB   = foldl1 (^^*)
{-# INLINE xorB  #-}

nandB :: (Foldable t, ExtraB b) => t b -> b
nandB  = foldl1 (&&~)
{-# INLINE nandB #-}

norB  :: (Foldable t, ExtraB b) => t b -> b
norB   = foldl1 (||~)
{-# INLINE norB  #-}

-- | A parity function?
xnorB :: (Foldable t, ExtraB b) => t b -> b
xnorB  = foldl1 (^^~)
{-# INLINE xnorB #-}


-- * Miscellaneous functions.
------------------------------------------------------------------------------
-- | Do the lengths of the given containers match?
lenB :: (Foldable s, Foldable t, Boolean b) => s u -> t v -> b
lenB xs ys = bool false true $ length xs == length ys
{-# INLINE lenB #-}

-- | For the two input-bits, compute the sum and carry bits.
halfAdder :: BoolBit b => b -> b -> (b, b)
halfAdder a b = (a ^^* b, a &&* b)
{-# INLINE halfAdder #-}

fullAdder :: BoolBit b => b -> b -> b -> (b, b)
fullAdder a b c = let (x, d) = halfAdder a b
                      (y, e) = halfAdder c x
                  in  (y, d ||* e)
{-# INLINE fullAdder #-}
