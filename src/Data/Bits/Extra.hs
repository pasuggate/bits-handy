{-# LANGUAGE FlexibleContexts, FlexibleInstances, FunctionalDependencies,
             MultiParamTypeClasses, UndecidableInstances #-}

------------------------------------------------------------------------------
-- |
-- Module      : Data.Bits.Extra
-- Copyright   : (C) Patrick Suggate 2016
-- License     : BSD3
--
-- Maintainer  : Patrick Suggate <patrick.suggate@gmail.com>
-- Stability   : Experimental
-- Portability : non-portable
--
-- Some more Boolean functions.
--
-- Changelog:
--  + 14/03/2016  --  initial file;
--  + 24/04/2016  --  some refactoring to add support for more types and
--                    operations;
--
-- TODO:
--  + build some testbenches;
--  + inlines useful?
--  + are there primitive GHC operations for shifts with `Word8` amounts?
--
------------------------------------------------------------------------------

module Data.Bits.Extra
  (
    module Data.Bits

  , ExtraB (..)
  , ShiftB (..), (%>>), (%<<)
  , ShuffleB (..), ReverseB (..)
  , BitwiseB (..)

    -- Additional bitwise functions
  , tzcB
  , ones32
  , ones64
  , msb32
  , msb64

    -- Helpers
  , fi
  )
where

import           Data.Bits
import           Data.Boolean.Extra
import           Data.Int
import           Data.Word
import           Foreign.C.Types


-- * Additional bitwise functionality.
------------------------------------------------------------------------------
-- | Interleaves the bits from the upper-half of the word with those of the
--   lower-half, and `unshuffle` reverses this, so that:
--
--     unshuffle . shuffle  ==  id
--
class ShuffleB b where
  shuffle   :: b -> b
  unshuffle :: b -> b

------------------------------------------------------------------------------
-- | Bit-reversal functionality, and such that:
--
--     bitrev . bitrev  ==  id
--
class ReverseB b where
  bitrev :: b -> b


-- * Experimental bitwise functionality.
------------------------------------------------------------------------------
-- | Various bitwise and mask-building operations.
--   TODO:
class BitwiseB b where
  msbmask :: b -> b
  ones    :: b -> b
  lzc     :: b -> b
  tzc     :: b -> b

-- | Make masks from words.
instance BitwiseB Word32 where
  msbmask  = msb32
  ones     = ones32
  lzc      = lzc32
  tzc      = tzc32

instance BitwiseB Word64 where
  msbmask  = msb64
  ones     = ones64
  lzc      = lzc64
  tzc      = tzc64

-- | Make masks from signed integral types.
instance BitwiseB Int32 where
  msbmask  = msb32
  ones     = ones32
  lzc      = lzc32
  tzc      = tzc32

instance BitwiseB Int64 where
  msbmask  = msb64
  ones     = ones64
  lzc      = lzc64
  tzc      = tzc64


-- * Some extra instances.
--   TODO: Use the CPP or TH to eliminate this boilerplate.
------------------------------------------------------------------------------
-- | Some word instances.
instance ExtraB Word where
  (^^*) = xor
  (&&~) = (complement .) . (.&.)
  (||~) = (complement .) . (.|.)
  (^^~) = (complement .) . xor

instance ExtraB Word8 where
  (^^*) = xor
  (&&~) = (complement .) . (.&.)
  (||~) = (complement .) . (.|.)
  (^^~) = (complement .) . xor

instance ExtraB Word16 where
  (^^*) = xor
  (&&~) = (complement .) . (.&.)
  (||~) = (complement .) . (.|.)
  (^^~) = (complement .) . xor

instance ExtraB Word32 where
  (^^*) = xor
  (&&~) = (complement .) . (.&.)
  (||~) = (complement .) . (.|.)
  (^^~) = (complement .) . xor

instance ExtraB Word64 where
  (^^*) = xor
  (&&~) = (complement .) . (.&.)
  (||~) = (complement .) . (.|.)
  (^^~) = (complement .) . xor

------------------------------------------------------------------------------
-- | Some integer instances.
instance ExtraB Int8 where
  (^^*) = xor
  (&&~) = (complement .) . (.&.)
  (||~) = (complement .) . (.|.)
  (^^~) = (complement .) . xor

instance ExtraB Int16 where
  (^^*) = xor
  (&&~) = (complement .) . (.&.)
  (||~) = (complement .) . (.|.)
  (^^~) = (complement .) . xor

instance ExtraB Int32 where
  (^^*) = xor
  (&&~) = (complement .) . (.&.)
  (||~) = (complement .) . (.|.)
  (^^~) = (complement .) . xor

instance ExtraB Int64 where
  (^^*) = xor
  (&&~) = (complement .) . (.&.)
  (||~) = (complement .) . (.|.)
  (^^~) = (complement .) . xor

------------------------------------------------------------------------------
-- | Some additional instances.
instance ExtraB CInt where
  (^^*) = xor
  (&&~) = (complement .) . (.&.)
  (||~) = (complement .) . (.|.)
  (^^~) = (complement .) . xor

instance ExtraB Int where
  (^^*) = xor
  (&&~) = (complement .) . (.&.)
  (||~) = (complement .) . (.|.)
  (^^~) = (complement .) . xor


-- ** Some shifter instances.
------------------------------------------------------------------------------
-- | Some word instances.
instance ShiftB Int Word where
  (<<%) = unsafeShiftL
  (>>%) = unsafeShiftR

instance ShiftB Int Word8 where
  (<<%) = unsafeShiftL
  (>>%) = unsafeShiftR

instance ShiftB Int Word16 where
  (<<%) = unsafeShiftL
  (>>%) = unsafeShiftR

instance ShiftB Int Word32 where
  (<<%) = unsafeShiftL
  (>>%) = unsafeShiftR

instance ShiftB Int Word64 where
  (<<%) = unsafeShiftL
  (>>%) = unsafeShiftR
  {-# INLINE (<<%) #-}
  {-# INLINE (>>%) #-}

------------------------------------------------------------------------------
-- | Some integer instances.
instance ShiftB Int Int where
  (<<%) = unsafeShiftL
  (>>%) = unsafeShiftR

instance ShiftB Int Int8 where
  (<<%) = unsafeShiftL
  (>>%) = unsafeShiftR

instance ShiftB Int Int16 where
  (<<%) = unsafeShiftL
  (>>%) = unsafeShiftR

instance ShiftB Int Int32 where
  (<<%) = unsafeShiftL
  (>>%) = unsafeShiftR

instance ShiftB Int Int64 where
  (<<%) = unsafeShiftL
  (>>%) = unsafeShiftR

------------------------------------------------------------------------------
-- | Some additional instances.
instance ShiftB Int CInt where
  (<<%) = unsafeShiftL
  (>>%) = unsafeShiftR

instance ShiftB Int Integer where
  (<<%) = unsafeShiftL
  (>>%) = unsafeShiftR


-- ** Shuffle operations.
------------------------------------------------------------------------------
-- | Adapted from:
--     http://aggregate.org/MAGIC/
instance ShuffleB Word32 where
  shuffle   = shuf32
  unshuffle = unsh32

instance ShuffleB Word64 where
  shuffle   = shuf64
  unshuffle = unsh64

------------------------------------------------------------------------------
-- | Integer versions.
instance ShuffleB Int32 where
  shuffle   = shuf32
  unshuffle = unsh32

instance ShuffleB Int64 where
  shuffle   = shuf64
  unshuffle = unsh64

------------------------------------------------------------------------------
-- TODO: Sensible?
instance ShuffleB Int where
  shuffle   = shuf64
  unshuffle = unsh64


-- ** Bit-reverse operations.
------------------------------------------------------------------------------
instance ReverseB Word32 where
  bitrev = br32

instance ReverseB Word64 where
  bitrev = br64

------------------------------------------------------------------------------
-- | Integer versions.
instance ReverseB Int32 where
  bitrev = br32

instance ReverseB Int64 where
  bitrev = br64


-- * Additional bitwise functions.
------------------------------------------------------------------------------
-- | Trailing Zero Count (TZC), see:
--     http://aggregate.org/MAGIC/
--   NOTE: Assumes that `a` is 2's complement.
tzcB :: (Num a, Bits a) => a -> Int
tzcB x = popCount $ x .&. negate x - 1
-- tzcB  = popCount . (+1) . arc (.&.) <<< id &&& negate
{-# INLINE tzcB #-}

tzc32 :: (Num a, Bits a, ShiftB Int a) => a -> a
tzc32 x = ones32 $ x .&. negate x - 1
{-# INLINE tzc32 #-}

tzc64 :: (Num a, Bits a, ShiftB Int a) => a -> a
tzc64 x = ones64 $ x .&. negate x - 1
{-# INLINE tzc64 #-}


-- * Additional, internal bitwise helper functions.
------------------------------------------------------------------------------
-- | Returns the word with just the MSB set.
--
--   From:
--     http://aggregate.org/MAGIC/
--
--   NOTE: This function doesn't return the count, or position, of the MSB,
--     just the word after all input bits have been cleared, except for the
--     MSB. This is different to Kmett's `msb`, from the `bits` package.
--
msb64 :: (Num a, Bits a, ShiftB Int a) => a -> a
msb64 x0 = x6 .&. complement (x6 >>% 1) where
-- msb64 x0 = ones64 x6 - 1 where
  x1 = x0 .|. x0 >>% 1
  x2 = x1 .|. x1 >>% 2
  x3 = x2 .|. x2 >>% 4
  x4 = x3 .|. x3 >>% 8
  x5 = x4 .|. x4 >>% 16
  x6 = x5 .|. x5 >>% 32
{-# INLINE msb64 #-}

lzc64 :: (Num a, Bits a, ShiftB Int a) => a -> a
lzc64 x0 = 64 - ones64 x6 where
  x1 = x0 .|. x0 >>% 1
  x2 = x1 .|. x1 >>% 2
  x3 = x2 .|. x2 >>% 4
  x4 = x3 .|. x3 >>% 8
  x5 = x4 .|. x4 >>% 16
  x6 = x5 .|. x5 >>% 32
{-# INLINE lzc64 #-}

msb32 :: (Num a, Bits a, ShiftB Int a) => a -> a
msb32 x0 = x5 .&. complement (x5 >>% 1) where
-- msb32 x0 = ones32 x6 - 1 where
  x1 = x0 .|. x0 >>% 1
  x2 = x1 .|. x1 >>% 2
  x3 = x2 .|. x2 >>% 4
  x4 = x3 .|. x3 >>% 8
  x5 = x4 .|. x4 >>% 16
{-# INLINE msb32 #-}

lzc32 :: (Num a, Bits a, ShiftB Int a) => a -> a
lzc32 x0 = 32 - ones32 x5 where
  x1 = x0 .|. x0 >>% 1
  x2 = x1 .|. x1 >>% 2
  x3 = x2 .|. x2 >>% 4
  x4 = x3 .|. x3 >>% 8
  x5 = x4 .|. x4 >>% 16
{-# INLINE lzc32 #-}


-- ** Internal shuffle functions.
------------------------------------------------------------------------------
-- | 32-bit shuffle operations. Adapted from:
--     http://aggregate.org/MAGIC/
shuf32 :: (Num a, Bits a, ShiftB Int a) => a -> a
shuf32 k0 = k4
  where
    k1 = (k0 .&. 0x0000FF00) <<% 8 .|.
         k0 >>% 8 .&. 0x0000FF00 .|. k0 .&. 0xFF0000FF
    k2 = (k1 .&. 0x00F000F0) <<% 4 .|.
         k1 >>% 4 .&. 0x00F000F0 .|. k1 .&. 0xF00FF00F
    k3 = (k2 .&. 0x0C0C0C0C) <<% 2 .|.
         k2 >>% 2 .&. 0x0C0C0C0C .|. k2 .&. 0xC3C3C3C3
    k4 = (k3 .&. 0x22222222) <<% 1 .|.
         k3 >>% 1 .&. 0x22222222 .|. k3 .&. 0x99999999
{-# INLINE shuf32 #-}

unsh32 :: (Num a, Bits a, ShiftB Int a) => a -> a
unsh32 k0 = k4
  where
    k4 = (k3 .&. 0x0000FF00) <<% 8 .|.
         k3 >>% 8 .&. 0x0000FF00 .|. k3 .&. 0xFF0000FF
    k3 = (k2 .&. 0x00F000F0) <<% 4 .|.
         k2 >>% 4 .&. 0x00F000F0 .|. k2 .&. 0xF00FF00F
    k2 = (k1 .&. 0x0C0C0C0C) <<% 2 .|.
         k1 >>% 2 .&. 0x0C0C0C0C .|. k1 .&. 0xC3C3C3C3
    k1 = (k0 .&. 0x22222222) <<% 1 .|.
         k0 >>% 1 .&. 0x22222222 .|. k0 .&. 0x99999999
{-# INLINE unsh32 #-}

------------------------------------------------------------------------------
-- | 64-bit shuffle operations. Adapted from:
--     http://aggregate.org/MAGIC/
shuf64 :: (Num a, Bits a, ShiftB Int a) => a -> a
shuf64 k0 = k5
  where
    k1 = (k0 .&. 0x00000000FFFF0000) <<% 16 .|.
         k0 >>% 16 .&. 0x00000000FFFF0000 .|. k0 .&. 0xFFFF00000000FFFF
    k2 = (k1 .&. 0x0000FF000000FF00) <<% 8  .|.
         k1 >>% 8  .&. 0x0000FF000000FF00 .|. k1 .&. 0xFF0000FFFF0000FF
    k3 = (k2 .&. 0x00F000F000F000F0) <<% 4  .|.
         k2 >>% 4  .&. 0x00F000F000F000F0 .|. k2 .&. 0xF00FF00FF00FF00F
    k4 = (k3 .&. 0x0C0C0C0C0C0C0C0C) <<% 2  .|.
         k3 >>% 2  .&. 0x0C0C0C0C0C0C0C0C .|. k3 .&. 0xC3C3C3C3C3C3C3C3
    k5 = (k4 .&. 0x2222222222222222) <<% 1  .|.
         k4 >>% 1  .&. 0x2222222222222222 .|. k4 .&. 0x9999999999999999
{-# INLINE shuf64 #-}

unsh64 :: (Num a, Bits a, ShiftB Int a) => a -> a
unsh64 k0 = k5
  where
    k5 = (k4 .&. 0x00000000FFFF0000) <<% 16 .|.
         k4 >>% 16 .&. 0x00000000FFFF0000 .|. k4 .&. 0xFFFF00000000FFFF
    k4 = (k3 .&. 0x0000FF000000FF00) <<% 8  .|.
         k3 >>% 8  .&. 0x0000FF000000FF00 .|. k3 .&. 0xFF0000FFFF0000FF
    k3 = (k2 .&. 0x00F000F000F000F0) <<% 4  .|.
         k2 >>% 4  .&. 0x00F000F000F000F0 .|. k2 .&. 0xF00FF00FF00FF00F
    k2 = (k1 .&. 0x0C0C0C0C0C0C0C0C) <<% 2  .|.
         k1 >>% 2  .&. 0x0C0C0C0C0C0C0C0C .|. k1 .&. 0xC3C3C3C3C3C3C3C3
    k1 = (k0 .&. 0x2222222222222222) <<% 1  .|.
         k0 >>% 1  .&. 0x2222222222222222 .|. k0 .&. 0x9999999999999999
{-# INLINE unsh64 #-}


-- ** Internal bit-reversal functions.
------------------------------------------------------------------------------
-- | Adapted from:
--     http://aggregate.org/MAGIC/
br32 :: (Num a, Bits a, ShiftB Int a) => a -> a
br32 k0 = k4 >>% 16 .|. k4 <<% 16
  where
    k1 = (k0 .&. 0xAAAAAAAA) >>% 1  .|. (k0 .&. 0x55555555) <<% 1
    k2 = (k1 .&. 0xCCCCCCCC) >>% 2  .|. (k1 .&. 0x33333333) <<% 2
    k3 = (k2 .&. 0xF0F0F0F0) >>% 4  .|. (k2 .&. 0x0F0F0F0F) <<% 4
    k4 = (k3 .&. 0xFF00FF00) >>% 8  .|. (k3 .&. 0x00FF00FF) <<% 8
{-# INLINE br32 #-}

-- | Adapted from:
--     http://aggregate.org/MAGIC/
br64 :: (Num a, Bits a, ShiftB Int a) => a -> a
br64 k0 = k5 >>% 32 .|. k5 <<% 32
  where
    k1 = (k0 .&. 0xAAAAAAAAAAAAAAAA) >>% 1  .|.
         (k0 .&. 0x5555555555555555) <<% 1
    k2 = (k1 .&. 0xCCCCCCCCCCCCCCCC) >>% 2  .|.
         (k1 .&. 0x3333333333333333) <<% 2
    k3 = (k2 .&. 0xF0F0F0F0F0F0F0F0) >>% 4  .|.
         (k2 .&. 0x0F0F0F0F0F0F0F0F) <<% 4
    k4 = (k3 .&. 0xFF00FF00FF00FF00) >>% 8  .|.
         (k3 .&. 0x00FF00FF00FF00FF) <<% 8
    k5 = (k4 .&. 0xFFFF0000FFFF0000) >>% 16 .|.
         (k4 .&. 0x0000FFFF0000FFFF) <<% 16
{-# INLINE br64 #-}


-- * Miscellaneous.
------------------------------------------------------------------------------
-- | 32-bit recursive reduction using SWAR, but first step is mapping 2-bit
--   values into sum of 2 1-bit values in sneaky way.
--
--   See:
--     http://aggregate.org/MAGIC/
--
ones32 :: (Num a, Bits a, ShiftB Int a) => a -> a
ones32 x0 = x5 .&. 0x0000003F where
  x1 = x0 - x0 >>% 1 .&. 0x55555555
  x2 = x1 >>% 2 .&. 0x33333333 + x1 .&. 0x33333333
  x3 = (x2 >>% 4 + x2) .&. 0x0F0F0F0F
  x4 = x3 + x3 >>% 8
  x5 = x4 + x4 >>% 16
{-# INLINE ones32 #-}

ones64 :: (Num a, Bits a, ShiftB Int a) => a -> a
ones64 x0 = x6 .&. 0x0000007F where
  x1 = x0 - x0 >>% 1 .&. 0x5555555555555555
  x2 = x1 >>% 2 .&. 0x3333333333333333 + x1 .&. 0x3333333333333333
  x3 = (x2 >>% 4 + x2) .&. 0x0F0F0F0F0F0F0F0F
  x4 = x3 + x3 >>% 8
  x5 = x4 + x4 >>% 16
  x6 = x5 + x5 >>% 32
{-# INLINE ones64 #-}

------------------------------------------------------------------------------
-- | Useful alias for the lazy.
fi :: (Integral i, Num a) => i -> a
fi  = fromIntegral
{-# INLINE fi #-}

{-- }
-- FIXME: Too general, as it causes overlapped instances.
instance (Boolean b, Bits b) => ExtraB b where
  (^^*) = xor
  (&&~) = (complement .) . (.&.)
  (||~) = (complement .) . (.|.)
  (^^~) = (complement .) . xor

instance Bits b => ShiftB Int b where
  (<<%) = unsafeShiftL
  (>>%) = unsafeShiftR
--}
