------------------------------------------------------------------------------
-- |
-- Module      : Data.Bits.Handy
-- Copyright   : (C) Patrick Suggate 2016
-- License     : BSD3
--
-- Maintainer  : Patrick Suggate <patrick.suggate@gmail.com>
-- Stability   : Experimental
-- Portability : non-portable
--
-- Some more Boolean functions.
--
-- Changelog:
--  + 14/03/2016  --  initial file;
--  + 24/04/2016  --  some refactoring to add support for more types and
--                    operations;
--
-- TODO:
--  + build some testbenches;
--  + inlines useful?
--  + are there primitive GHC operations for shifts with `Word8` amounts?
--
------------------------------------------------------------------------------

module Data.Bits.Handy
  (
    -- Re-exports
    module Data.Bits.Extra

    -- Convenience functions
  , int
  , i32
  , i64
  )
where

import           Data.Bits.Extra
import           GHC.Int


-- * Helper functions
------------------------------------------------------------------------------
int :: Integral i => i -> Int
int  = fromIntegral
{-# INLINE int #-}

i32 :: Integral i => i -> Int32
i32  = fromIntegral
{-# INLINE i32 #-}

i64 :: Integral i => i -> Int64
i64  = fromIntegral
{-# INLINE i64 #-}
