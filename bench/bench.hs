module Main where

import           Criterion.Main   (bench, bgroup, defaultMain, whnf)
import           Data.Bits.Extra
import           Data.Bits.Extras
import           GHC.Word


bitbench =
  bgroup "Bitwise operations"
  [ bench "popCount " $ whnf popCount x
  , bench "ones64   " $ whnf ones x
  , bench "lsb      " $ whnf lsb  x
  , bench "nlz      " $ whnf nlz  x
  , bench "lzc      " $ whnf lzc  x
  , bench "rank     " $ whnf rank x
  , bench "tzc      " $ whnf tzc  x
  , bench "msb      " $ whnf msb   x
  , bench "msb64    " $ whnf msb64 x
  ] where
    x = 0x3b1802a75ef296c4 :: Word64


main = do
  defaultMain
    [ bitbench
    ]
